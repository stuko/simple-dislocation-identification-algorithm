#!/bin/bash

../build/SDIA ../latticevectors/bcc.txt 3.15 bcc_example_1.dump bcc_example_1.vtk

../build/SDIA ../latticevectors/fcc_partials.txt 3.6 fcc_example_1.dump fcc_example_1.vtk

../build/SDIA ../latticevectors/fcc_partials.txt 3.62 fcc_example_2.dump fcc_example_2.vtk "[1 1 -2, -1 1 0, 1 1 1]"

