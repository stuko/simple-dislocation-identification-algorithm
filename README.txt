****************************************************************************************
Simple Dislocation Identification Algorithm 
****************************************************************************************

Version: 1.0

Author: Alexander Stukowski (stukowski@mm.tu-darmstadt.de)
        (Institute of Materials Science, Darmstadt University of Technology, Germany)
        
This source code provides a reference implementation of the simple dislocation identification
algorithm described in the paper:

  A. Stukowski,
  "A triangulation-based method to identify dislocations in atomistic models"
  Journal of the Mechanics and Physics of Solids 70 (2014), 314–319
  DOI: 10.1016/j.jmps.2014.06.009
  
A copy of the paper is included with this package (Stukowski_JMPS_2014.pdf).

The code can read and analyze snapshots from three-dimensional molecular dynamics 
simulations of fcc and bcc crystals. 

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version. See LICENSE.txt for more information.

****************************************************************************************
Requirements 
****************************************************************************************

The code has been developed and tested on Linux and Mac OS X. Compiling the code on Windows
should work too, but probably requires modified build steps.

The following is required to compile the code:

- C++ compiler (C++11 compatibility required)
- CMake (see www.cmake.org)
- Computational Geometry Algorithms Library, at least version 4.0 (see www.cgal.org)

- ParaView (for visualizing the results, see www.paraview.org)

****************************************************************************************
Build instructions 
****************************************************************************************

- Create an empty build directory (e.g. below the directory containing this readme file)

- From within that directory, run 
   
    cmake ..
    
  where ".." is the path to the directory containing the file CMakeLists.txt.
  CMake should identify the C++ compiler on your system and find the CGAL library.
  If the library cannot be automatically found, run "ccmake ." now to specify
  its location by setting the corresponding build configuration variable.

- Run "make" to compile the program. The resulting executable is called 'SDIA'.

****************************************************************************************
Usage 
****************************************************************************************

The "examples/" subdirectory contains some examples to demonstrate the usage of the program.
Have a look at the script file "run.sh".

The program is invoked as follows:

 SDIA <lattice_file> <lattice_constant> <input.dump> <output.vtk> "[lattice_orientation]"
 
The <lattice_file> parameter specifies the path of a text file that contains the list
of ideal lattice vectors. The "latticevectors" subdirectory already contains corresponding 
files for the BCC ('bcc.txt') and FCC ('fcc_partials.txt') lattices. 

<lattice_constant> specifies the lattice parameter of the crystal being analyzed.
All vectors read from the lattice file get multiplied by this value.

The <input.dump> parameter is the path to the input file to be analyzed. The program can
read LAMMPS text dump files. The program only reads in the "x", "y", and "z" file colums
from the file. Periodic boundary conditions are currently not supported and will be ignored.

The <output.vtk> parameter specifies the name of the output file, which will be written
by the program. The generated file can be opened with the visualization software ParaView
and contains all Delaunay cells that are intersected by a dislocation.

By default, the program assumes that the simulated crystal is oriented such that the x,y, 
and z axes of the simulation coordinate system coincide with the [100], [010], and [001] 
axes of the crystal. For rotated crystals, the orientation must be specified using the
last, optional command line parameter, which must be a string containing nine numbers.
The string must be formatted exactly as in the following example. For instance, 

  "[1 1 1, 1 1 -2, -1 1 0]"
  
would describe a cubic crystal with the [111] direction aligned along the x-axis of the simulation,
the [11-2] crystal direction aligned along the y-axis, and [-110] along z. Note that the 
three lattice vectors must form a right-handed orthogonal coordinate system (otherwise the 
program will quit with an error message). The program uses the derived orientation matrix 
to transform the ideal lattice vectors read from the lattice file (which are specified in 
the usual Bravais cell orientation).

****************************************************************************************
Change Log 
****************************************************************************************

Version 1.0 (21-Jul-14):

 - This is the initial public release of the code.