///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (2014) Alexander Stukowski (stukowski@mm.tu-darmstadt.de)
//
//  This file is part of the program code 'Simple Dislocation Identification Algorithm'
//  that accompanies the paper:
//
//  A. Stukowski,
//  "A triangulation-based method to identify dislocations in atomistic models"
//  Journal of the Mechanics and Physics of Solids 70 (2014), 314–319
//  DOI: 10.1016/j.jmps.2014.06.009
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
///////////////////////////////////////////////////////////////////////////////

#include "SDIA.h"

/**
 * This is the entry point of the simple analysis program.
 *
 * The main() function is responsible for parsing the command line parameters and
 * invoking the high-level I/O and analysis routines in the right
 * order.
 */
int main(int argc, char** argv)
{
	// Parse command line parameters.
	if(argc < 5) {
		cerr << "Program expects at least four command line parameters:" << endl << endl;
		cerr << "SDIA <lattice_file> <lattice_constant> <input.dump> <output.vtk> \"[lattice_orientation]\"" << endl << endl;
		return 1;
	}

	try {

		// This is the main object performing the analysis.
		SDIA sdia;

		// Get user-defined lattice parameter from command line.
		double latticeParameter = atof(argv[2]);
		if(latticeParameter <= 0) {
			cerr << "Invalid lattice parameter: " << argv[2] << endl;
			return 1;
		}

		// The lattice orientation. Assume identity matrix by default.
		double orientation[3][3] = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};

		// Parse lattice orientation matrix.
		if(argc > 5) {
			int n = sscanf(argv[5], "[%lg %lg %lg, %lg %lg %lg, %lg %lg %lg]",
					&orientation[0][0], &orientation[0][1], &orientation[0][2],
					&orientation[1][0], &orientation[1][1], &orientation[1][2],
					&orientation[2][0], &orientation[2][1], &orientation[2][2]);
			if(n != 9) throw runtime_error("Invalid lattice orientation. Syntax error.");
		}

		// Load list of ideal lattice vectors from text file.
		sdia.parseLatticeVectors(argv[1]);

		// Precompute vector lists.
		sdia.prepare(latticeParameter, orientation);

		// Load input simulation data.
		sdia.parseDumpFile(argv[3]);

		// Generate Delaunay tessellation.
		sdia.generateTessellation();

		// Assign ideal lattice vectors to tessellation edges.
		sdia.assignLatticeVectors();

		// Optimize lattice vectors.
		sdia.optimizeEdges();

		// Detect dislocated tessellation cells.
		sdia.markDislocatedCells();

		// Write output file.
		sdia.writeVTKFile(argv[4]);
	}
	catch(const std::exception& ex) {
		cerr << "ERROR: " << ex.what() << endl;
		return 1;
	}

	return 0;
}
